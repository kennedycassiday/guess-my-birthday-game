from random import randint

# ask the human for name
name = input("Hi, what's your name?")

# Guess

for guess_number in range(1, 6):

    month = randint(1, 12)
    year = randint(1924, 2004)

    print(guess_number, ":", name, "were you born in",
    month, "/", year,"?")

    #is the guess correct?#
    answer = input("yes or no?")

    if answer == "yes" :
        print("I knew it!")
        exit()
    elif answer == "no" and guess_number < 5 :
        print("Drat! Lemme try again!")
    else :
        print("I have other things to do. Good bye.")
